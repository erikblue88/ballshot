﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public GameObject bullet;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Token") || collision.CompareTag("Obstacle"))     //If gameobject collides with Token or an Obstacle
            Destroy(collision.gameObject);        //Destroys gameobject

        else if (collision.CompareTag("Bullet") || collision.CompareTag("PlusBullet"))      //If gameobject collides with a Bullet or with a PlusBullet
        {
            FindObjectOfType<Control>().countOfBalls++;
            FindObjectOfType<Control>().countOfPowerUps++;
            GetComponent<Collider2D>().enabled = false;
            Invoke("SpawnBullet", 0.2f);
            Destroy(gameObject, 0.21f);        //Destroys gameobject
        }
    }

    public void SpawnBullet()
    {
        GameObject tempBullet = Instantiate(bullet, transform.position, Quaternion.identity);       //Spawns a bullet
        tempBullet.GetComponent<Rigidbody2D>().AddForce(new Vector3(Random.Range(-1f, 1f), 1f, 0f) * 10000f);        //Shoots the bullet upwards
    }
}
