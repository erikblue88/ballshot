﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Control : MonoBehaviour
{
    public GameObject bullet, speedButton, ballImage;
    public TextMeshProUGUI ballCounterText;
    public float phoneSpeed = 0.5f, computerSpeed, reloadTime, bulletSpeed;
    public int countOfBalls;

    [HideInInspector]
    public int countOfPowerUps = 0;

    private Touch initTouch = new Touch();
    private Rigidbody2D rb;
    private float positionX, positionY;
    private Vector3 originPos;
    private bool touching = false, canShoot = false;
    private GameObject trajectory;

    [HideInInspector]
    public int tempBallCount, shootableBalls;

    void Start()
    {
        //Initializations
        rb = GetComponent<Rigidbody2D>();
        positionX = 0f;
        positionY = transform.localPosition.y;
        originPos = transform.localPosition;
        trajectory = transform.GetChild(0).gameObject;

        trajectory.SetActive(false);

        UpdateBalls();
        UpdateBallCounterText();
    }

    void Update()
    {
        if ((Input.touchCount > 0) && canShoot)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)        //if finger touches the screen
            {
                if (touching == false)
                {
                    touching = true;
                    initTouch = Input.GetTouch(0);
                }
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved)       //if finger moves while touching the screen
            {
                float deltaX = initTouch.position.x - Input.GetTouch(0).position.x;
                float rotation =+ deltaX * phoneSpeed * Time.deltaTime;
                float tempRotation = transform.rotation.eulerAngles.z + rotation;

                transform.rotation = Quaternion.Euler(0f, 0f, Mathf.Clamp(tempRotation, 6f, 174f));

                initTouch = Input.GetTouch(0);
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)       //if finger releases the screen
            {
                initTouch = new Touch();
                touching = false;

                speedButton.SetActive(true);
                trajectory.SetActive(false);
                canShoot = false;
                Shoot();
            }
        }

        //if you play on computer---------------------------------
        float rotation2 = 0f;
        if (canShoot)
        {
            if (Input.GetKey(KeyCode.A))
                rotation2 =- computerSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.D))
                rotation2 =+ computerSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.Space))
            {
                speedButton.SetActive(true);
                trajectory.SetActive(false);
                canShoot = false;
                Shoot();
            }

            float tempRotation2 = transform.rotation.eulerAngles.z + rotation2;

            transform.rotation = Quaternion.Euler(0f, 0f, Mathf.Clamp(tempRotation2, 6f, 174f));
        }
        //--------------------------------------------------------
    }

    public void Shoot()
    {
        tempBallCount--;

        UpdateBallCounterText();

        GameObject tempBullet = Instantiate(bullet, transform.position, Quaternion.identity);
        tempBullet.GetComponent<Rigidbody2D>().AddForce(transform.right * bulletSpeed);

        if (tempBallCount > 0)
            Invoke("Shoot", reloadTime);
        else
            ballImage.SetActive(false);
    }

    public void CanShootAgain()
    {
        trajectory.SetActive(true);
        canShoot = true;
    }

    public void UpdateBallCounterText()
    {
        ballCounterText.text = tempBallCount.ToString() + "x";
    }

    public void UpdateBalls()
    {
        shootableBalls = tempBallCount = countOfBalls;
        ballImage.SetActive(true);
    }

    public void SpeedButton()
    {
        Time.timeScale = 2f;
        speedButton.SetActive(false);
    }
}