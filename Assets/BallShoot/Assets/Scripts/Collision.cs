﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    public float maxVelocity;
    public Sprite skin1, skin2, skin3;

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();       //Initializates rigidbody

        SkinCheck();        //Checks skin
    }

    void Update()
    {
        if (rb.velocity.magnitude > maxVelocity)        //If the gameobject moves too fast
            rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxVelocity);     //Limits the speed of the gameobject
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))        //If gameobject collides with an Obstacle
        {
            collision.gameObject.GetComponent<Obstacle>().Damage();
        }
        else if (collision.gameObject.CompareTag("Token"))        //If gameobject collides with a Token
        {
            FindObjectOfType<ScoreManager>().IncrementToken();      //Increments tokencounter
            collision.gameObject.GetComponent<ParticleSystem>().Play();     //Plays particle of the Token
            collision.gameObject.GetComponent<Collider2D>().enabled = false;        //Disables the collider of the Token
            collision.gameObject.GetComponent<SpriteRenderer>().enabled = false;        //Disables the sprite of the Token
            Destroy(collision.gameObject, 2f);      //Destroys Token after x seconds
        }
    }

    public void SkinCheck()
    {
        //Selects a skin for the gameobject
    
        switch (PlayerPrefs.GetInt("Skin", 0))
        {
            case 0:
                GetComponent<SpriteRenderer>().sprite = skin1;
                break;
            case 1:
                GetComponent<SpriteRenderer>().sprite = skin2;
                break;
            case 2:
                GetComponent<SpriteRenderer>().sprite = skin3;
                break;
        }
    }
}
