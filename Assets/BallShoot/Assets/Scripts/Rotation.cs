﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public float minRotationSpeed, maxRotationSpeed;

    private float rotationSpeed;

    void Start()
    {
        rotationSpeed = Random.Range(minRotationSpeed, maxRotationSpeed);       //Selects a random rotationSpeed
        if (Random.Range(0, 2) == 0)
            rotationSpeed *= -1f;
    }

    void Update()
    {
        transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);      //Rotates the gameobject on the Z axis    
    }
}
