﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Obstacle : MonoBehaviour
{
    public GameObject explosionParticle;
    public float movementSpeed;
    public TextMeshPro hpText;

    [HideInInspector]
    public int hp = 10;

    private Animation anim;
    private Rigidbody2D rb;
    private SpriteRenderer rend;
    private Transform parentTransform;
    private bool canMove = true;
    private float obstacleTarget;

    void Start()
    {
        //Initializations
        rb = GetComponent<Rigidbody2D>();
        rend = GetComponent<SpriteRenderer>();
        parentTransform = transform.parent;
        anim = transform.parent.GetComponent<Animation>();

        //HP selection
        hp = FindObjectOfType<Control>().countOfBalls;
        hp += Mathf.RoundToInt(Random.Range(-hp / 3f, hp / 0.35f));

        hpText.text = hp.ToString();        //Writes out HP

        FindObjectOfType<ColorManager>().Coloring(rend, hp);        //Selects a color for the obstacle

        obstacleTarget = parentTransform.position.y - 1.15f;
    }

    void Update()
    {
        if (canMove)
        {
            parentTransform.Translate(Vector3.up * movementSpeed * Time.deltaTime);     //Moves the obstacle upwards

            if (parentTransform.position.y <= obstacleTarget)
                canMove = false;
        }
    }

    public void MoveDown(float moveDownBy)
    {
        obstacleTarget = parentTransform.position.y - moveDownBy;
        canMove = true;
    }

    public void Damage()
    {
        hp--;       //Decreases HP of the obstacle
        FindObjectOfType<ScoreManager>().IncrementScore();        //Increments score

        anim.Play("DamagedAnim");       //Plays animation
        FindObjectOfType<ColorManager>().Coloring(rend, hp);        //Changes color

        if (hp <= 0)        //If the obstacle needs to be destroyed
        {
            GameObject tempParticle = Instantiate(explosionParticle, transform.position, Quaternion.identity);      //Spawns an "ExplosionParticle"
            FindObjectOfType<ColorManager>().RandomColor(tempParticle.GetComponent<ParticleSystem>());      //Selects a color for the particle
            Destroy(tempParticle, 4f);      //Destroys particle
            Destroy(transform.parent.gameObject);        //Destroys obstacle
        }

        hpText.text = hp.ToString();        //Writes out HP
    }
}
