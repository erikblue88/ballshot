﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public Color[] colors;
    public float transition;

    public void Coloring(SpriteRenderer obj, int hp)
    {
        if (hp <= 4)
            obj.color = Color.Lerp(colors[0], colors[1], hp / transition);
        else if ((hp > 4) && (hp <= 8))
            obj.color = Color.Lerp(colors[1], colors[2], hp / transition);
        else if ((hp > 8) && (hp <= 12))
            obj.color = Color.Lerp(colors[2], colors[3], hp / transition);
        else if ((hp > 12) && (hp <= 18))
            obj.color = Color.Lerp(colors[3], colors[4], hp / transition);
        else if ((hp > 18) && (hp <= 24))
            obj.color = Color.Lerp(colors[4], colors[5], hp / transition);
        else if ((hp > 24) && (hp <= 35))
            obj.color = Color.Lerp(colors[5], colors[6], hp / transition);
        else if ((hp > 35) && (hp <= 55))
            obj.color = Color.Lerp(colors[6], colors[7], hp / transition);
        else if ((hp > 55) && (hp <= 85))
            obj.color = Color.Lerp(colors[7], colors[8], hp / transition);
        else if (hp > 85)
            obj.color = Color.Lerp(colors[8], colors[9], hp / transition);
    }

    public void RandomColor(ParticleSystem par)
    {
#pragma warning disable CS0618 // Type or member is obsolete
        par.startColor = colors[Random.Range(0, colors.Length)];
#pragma warning restore CS0618 // Type or member is obsolete
    }
}
