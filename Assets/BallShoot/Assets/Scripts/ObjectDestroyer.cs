﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour {

    private int countOfDestroyedBalls = 0, countOfDestroyedPowerUps = 0;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))        //If gameobject collides with a Bullet
        {
            Destroy(collision.gameObject);      //Destroys Bullet
            countOfDestroyedBalls++;

            if ((FindObjectOfType<Control>().shootableBalls <= countOfDestroyedBalls) && (FindObjectOfType<Control>().countOfPowerUps <= countOfDestroyedPowerUps))     //If there are no more balls
                NextRound();
        }
        else if (collision.CompareTag("PlusBullet"))        //If gameobject collides with a PlusBullet
        {
            Destroy(collision.gameObject);
            countOfDestroyedPowerUps++;

            if ((FindObjectOfType<Control>().shootableBalls <= countOfDestroyedBalls) && (FindObjectOfType<Control>().countOfPowerUps <= countOfDestroyedPowerUps))     //If there are no more balls
                NextRound();
        }
    }

    public void NextRound()
    {
        if (Time.timeScale != 1f)
            Time.timeScale = 1f;        //Resets time

        FindObjectOfType<Control>().speedButton.SetActive(false);
        FindObjectOfType<Control>().countOfPowerUps = 0;
            
        //Moves gameobjects downwards
        foreach (GameObject obstacle in GameObject.FindGameObjectsWithTag("Obstacle"))
            obstacle.GetComponent<Obstacle>().MoveDown(1.15f);
        foreach (GameObject obstacle in GameObject.FindGameObjectsWithTag("Token"))
            obstacle.GetComponent<DownMovement>().MoveDown(1.15f);
        foreach (GameObject obstacle in GameObject.FindGameObjectsWithTag("PowerUp"))
            obstacle.GetComponent<DownMovement>().MoveDown(1.15f);

        countOfDestroyedPowerUps = countOfDestroyedBalls = 0;
        FindObjectOfType<Spawner>().Spawn();        //Spawns new obstacles/tokens/powerups

        FindObjectOfType<Control>().CanShootAgain();
        FindObjectOfType<Control>().UpdateBalls();
        FindObjectOfType<Control>().UpdateBallCounterText();
    }
}
