﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject obstacle;
    public Transform[] spawnPositions;
    public GameObject token, powerUp;
    public int tokenFrequency = 3, powerUpFrequency = 3;

    public void Spawn()
    {
        if (!FindObjectOfType<GameManager>().gameIsOver)        //If the game is not over yet
        {
            for (int i = 0; i < spawnPositions.Length; i++)
            {
                if (Random.Range(0, 3) == 0)
                    Instantiate(obstacle, spawnPositions[i].position, Quaternion.identity);        //Spawns Obstacle to the position of the SpawnPoint

                else
                {
                    if (Random.Range(0, tokenFrequency) == 0)       //If it is time to spawn a Token
                        Instantiate(token, spawnPositions[i].position, Quaternion.identity);        //Spawns a Token to the position of the SpawnPoint
                    if (Random.Range(0, powerUpFrequency) == 0)       //If it is time to spawn a PowerUp
                        Instantiate(powerUp, spawnPositions[i].position, Quaternion.identity);        //Spawns a PowerUp to the position of the SpawnPoint
                }
            }
        }
    }
}
