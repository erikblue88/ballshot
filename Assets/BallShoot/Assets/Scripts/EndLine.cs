﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLine : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Obstacle"))        //If gameobject collides with an Obstacle
            FindObjectOfType<GameManager>().EndPanelActivation();       //Then the game is over
    }
}
