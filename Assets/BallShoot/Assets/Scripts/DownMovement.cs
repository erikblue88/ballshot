﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownMovement : MonoBehaviour
{
    public float movementSpeed;

    private bool canMove = true;
    private float obstacleTarget;

    void Start()
    {
        obstacleTarget = transform.position.y - 1.15f;
    }

    void Update()
    {
        if (canMove)
        {
            transform.Translate(Vector3.up * movementSpeed * Time.deltaTime);       //Moves the gameobject downwards

            if (transform.position.y <= obstacleTarget)
                canMove = false;
        }
    }

    public void MoveDown(float moveDownBy)
    {
        obstacleTarget = transform.position.y - moveDownBy;
        canMove = true;
    }
}
